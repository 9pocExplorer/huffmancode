#pragma once
#include <iostream>
#include "heap.h"

using namespace std;


template <typename E> class HuffNode{
public:
virtual ~HuffNode(){

};
virtual int Weight()=0;
virtual bool IsLeaf()=0;
};

template <typename E> class LeafNode : public HuffNode<E>{
private:
    E it;
    int wgt;
public:
    LeafNode(const E& val, int freq){
        it=val;
        wgt=freq;
    }
    int Weight(){
        return wgt;
    }
    E Val(){
        return it;
    }
    bool IsLeaf(){
        return true;
    }

};

template <typename E>
class IntlNode : public HuffNode<E>{
private:
    HuffNode<E>* lc;
    HuffNode<E>* rc;
    int wgt;
public:
    IntlNode(HuffNode<E>* l, HuffNode<E>* r){
        wgt=l->Weight()+r->Weight();
        lc = l;
        rc = r;
    }
    int Weight(){
        return wgt;
    }
    bool IsLeaf(){
        return false;
    }
    HuffNode<E>* Left() const
    {
        return lc;
    }
    void SetLeft(HuffNode<E>* b )
    {
        lc = (HuffNode<E>*)b;
    }
    HuffNode<E>* Right() const
    {
        return rc;
    }
    void SetRight(HuffNode<E>* b )
    {
        rc = (HuffNode<E>*)b;
    }
};

//创建哈夫曼树
template<class E>
class HuffTree{
private :
    HuffNode<E>*root;
public:

    HuffTree(E  val,int freg){//初始化时，创建的是叶节点
        root=new LeafNode<E>(val,freg);
    }
    HuffTree(HuffTree<E>* l,HuffTree<E> * r){
        root =new IntlNode<E>(l->root,r->root);
    }
    ~HuffTree(){

    }
    HuffNode<E> * Root(){
        return  root;
    }
    int RootWgt(){
        return root->Weight();
    }

};

template<class E>
class MinTreeComp{
public:
    bool prior(HuffTree<E> *tree1,HuffTree<E>* tree2){
        if(tree1->RootWgt()<tree2->RootWgt()){
            return true;
        }
        else{
            return false;
        }
    }

};

template<class E >
HuffTree<E>* BuildHuff(HuffTree<E>** treeArr,int count){//将一个装有许多哈夫曼树的数组传入，构建一颗哈夫曼树并返回该树
    Heap<HuffTree<E>*, MinTreeComp<E>>* forest= new Heap<HuffTree<E>*, MinTreeComp<E>>(treeArr,count,count);

    HuffTree<char> * temp1,* temp2,*temp3=NULL;
    while(forest->Size()>1){
        temp1=forest->RemoveFirst();
        temp2=forest->RemoveFirst();
        temp3=new HuffTree<E>(temp1,temp2);
        forest->Insert(temp3);
        delete temp1; //为什么要删除，是因为，在构建新的哈夫曼树的时候，不是简单的将之前的两颗数连在新树上，而是将之前树上的节点给创建的新树
        //最为该新数的孩子。因此之前的两颗数已经没必要留下来，避免产生内存泄漏，因此要释放该树的空间。
        delete temp2;
    }
    return temp3;
}

