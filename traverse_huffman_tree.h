//
// Created by Turing on 2022/11/10.
//
#pragma once
#include<string>
#include"my_vector.h"
#include"huff_tree.h"

void TraverseHuffTree(HuffNode<char>* huffNode,MyVector<char>&charContainer,MyVector<string>&codeContainer);

