//
// Created by Turing on 2022/11/7.
//
#include<iostream>
#include<fstream>
#include"huff_tree.h"
#include<string>
#include"my_vector.h"
#include"traverse_huffman_tree.h"
#include"compress.h"


using namespace std;


void Compress(char *filePath)
{
    ifstream sourceCode;
    sourceCode.open(filePath,ios::in);
    if(!sourceCode.is_open()){
        cout<<"打开失败\n";
        exit(1);
    }
    string temp;
    for(int i=0;sourceCode.peek()!=EOF;i++){
        temp.append(1,sourceCode.get());
    }

//    cout<<temp<<endl;  //测试读入的数据是否正确。

    sourceCode.close();
    MyVector<char>charElem;//装的字符
    MyVector<int >wgt;//装的相应的权重

    for(int i=0;i<temp.length();i++){
        int test=charElem.FindElem(temp[i]);
        if(test==-1){
            charElem.append(temp[i]);
            wgt.append(1);
        }
        else{
            wgt.AddOneAt(test);
        }
    }
//    for(int i=0;i<charElem.getNum();i++){
//        printf("the letter is %c, the weight is %d\n",charElem[i],wgt[i]);
//    }
    HuffTree<char>* treeArr[100]={0};

    for(int i=0;i<charElem.getNum();i++){
        treeArr[i]=new HuffTree<char>(charElem[i],wgt[i]);
    }
    HuffTree<char>* myHuffTree=BuildHuff<char>(treeArr,charElem.getNum());
    MyVector<string>codeContainer;//装的编码
    MyVector<char>charContainer;//装的对应编码的字符
    TraverseHuffTree(myHuffTree->Root(),charContainer,codeContainer);
//    for(int i=0;i<charContainer.getNum();i++){
//        printf("letter: %c",charContainer[i]);
//        cout<<"     code:" <<codeContainer[i]<<endl;
//    }
    //实现了编码

    cout<<"Please input target file name:";
    char tempStr[20]={0};
    cin>>tempStr;
    ofstream ofs;
    ofs.open(tempStr,ios::binary|ios::out);
    if(!ofs.is_open()){
        cout<<"文件打开失败"<<endl;
    }

    int leafNodeNum=charElem.getNum();
    ofs.write((char *)&leafNodeNum,sizeof(int));//写入编码需要的字符数量.
    //写入每个字符和对应得的权重.以一个字符一个权重的方式写入.
    for(int i=0;i<charElem.getNum();i++){
        char tempCh=charElem[i];
        ofs.write((char*)(&tempCh),sizeof(char));
        int tempWgt=wgt[i];
        ofs.write((char*)&tempWgt,sizeof(int));
    }
    int operationCount=0;//统计总共需要取多少次二进制位
    int countPerChar=0;//记录是否到达八个bit
    char tempCharForCode=0;//每个字节的临时记录
    MyVector<char>finalCode;
    for(int i=0;i<temp.length();i++){
        int tempPos=charContainer.FindElem(temp[i]);
        string tempCodeStr=codeContainer[tempPos];
        for(int j=0;j<tempCodeStr.length();j++){
            if(countPerChar==8){
                finalCode.append(tempCharForCode);
//                cout<<"the letter code is ";
                for(int k=0;k<8;k++){
//                    cout<<((tempCharForCode>>7) & 0x01);
                    tempCharForCode=tempCharForCode<<1;
                }
                cout<<endl;
                tempCharForCode=0;
                countPerChar=0;
            }
            tempCharForCode <<=1;
            char tempCh=tempCodeStr[j];
            if(tempCh=='0'){
                tempCharForCode=tempCharForCode&0xFE;
            }
            else{
                tempCharForCode=tempCharForCode|0x01;
            }
            operationCount++;
            countPerChar++;
        }
    }
    if(countPerChar!=0){
        finalCode.append(tempCharForCode);
        for(int k=0;k<countPerChar;k++){
            tempCharForCode=tempCharForCode<<1;
        }
    }
    ofs.write((char*)&operationCount,sizeof(int));
    for(int i=0;i<finalCode.getNum();i++){
        char tempCh;
        tempCh=finalCode[i];
        ofs.write((char*)&tempCh,sizeof(char));
    }
    ofs.close();
}
