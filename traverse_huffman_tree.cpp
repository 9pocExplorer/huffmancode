//
// Created by Turing on 2022/11/10.
//

#include"huff_tree.h"
#include<string>
#include"my_vector.h"
#include "traverse_huffman_tree.h"
string tempStr(1,'0');

void TraverseHuffTree(HuffNode<char>* huffNode,MyVector<char>&charContainer,MyVector<string>&codeContainer){
    if(huffNode->IsLeaf()){
        charContainer.append(((LeafNode<char>*)huffNode)->Val());
        codeContainer.append(tempStr);
        tempStr.pop_back();
        return ;
    }
    tempStr.append(1,'0');
    TraverseHuffTree(((IntlNode<char>*)huffNode)->Left(),charContainer,codeContainer);
    tempStr.append(1,'1');
    TraverseHuffTree(((IntlNode<char>*)huffNode)->Right(),charContainer,codeContainer);
    tempStr.pop_back();
}


