//
// Created by Turing on 2022/11/13.
//
#include<iostream>
#include"decompress.h"
#include<fstream>
#include"my_vector.h"
#include"huff_tree.h"

using namespace std;


void Decompress(char *sourcePath){
    ifstream ifs(sourcePath,ios::binary);
    cout<<"Please input target file name:";
    cout<<"Please input target file name:";
    char tempStr[20]={0};
    cin>>tempStr;
    ofstream ofs(tempStr,ios::out);
    int charForTreeNum;
    ifs.read((char*)&charForTreeNum,sizeof(int));
    MyVector<char>charElem;
    MyVector<int>wgt;
    for(int i=0;i<charForTreeNum;i++){
        char tempCh;
        int tempWgt;
        ifs.read(&tempCh,sizeof(char));
        charElem.append(tempCh);
        ifs.read((char*)&tempWgt,sizeof(int));
        wgt.append(tempWgt);
    }

    HuffTree<char>* treeArr[100]={0};

    for(int i=0;i<charElem.getNum();i++){
        treeArr[i]=new HuffTree<char>(charElem[i],wgt[i]);
    }
    HuffTree<char>* myHuffTree=BuildHuff<char>(treeArr,charElem.getNum());
    int huffTreeCodeNum;
    ifs.read((char *)&huffTreeCodeNum,sizeof(int));

    MyVector<char>tempCharContainer;
    int countForChar;
    int leftNum=0;
    if(huffTreeCodeNum%8==0){
        countForChar=huffTreeCodeNum/8;
    }
    else{
        countForChar=huffTreeCodeNum/8;
        leftNum=huffTreeCodeNum%8;
    }
    for(int i=0;i<countForChar;i++){
        char tempCh;
        ifs.read(&tempCh,sizeof(char));
        tempCharContainer.append(tempCh);
    }
    char charWriteToTxt;
    HuffNode<char>*tempNode=myHuffTree->Root();
    int i;
    for( i=0;i<countForChar;i++){
        char tempCh=tempCharContainer[i];

        for(int j=0;j<8;j++){
            if((i==0&&j==0)){
                tempCh=tempCh<<1;
                continue;
            }
            if(tempNode->IsLeaf()){
                charWriteToTxt=((LeafNode<char>*)tempNode)->Val();
                ofs<<charWriteToTxt;
                tempNode=myHuffTree->Root();

                tempCh=tempCh<<1;
                continue;
            }
            if(((tempCh>>7)&0x01)==0x01){
                tempNode=((IntlNode<char>*)tempNode)->Right();
                tempCh=tempCh<<1;
            }
            else{
                tempNode=((IntlNode<char>*)tempNode)->Left();
                tempCh=tempCh<<1;
            }
        }
    }

    if(leftNum!=0){
        char tempCh;
        ifs.read(&tempCh,sizeof(char));
        for(int j=0;j<leftNum+1;j++){
            if(tempNode->IsLeaf()){
                charWriteToTxt=((LeafNode<char>*)tempNode)->Val();
                ofs<<charWriteToTxt;
                tempNode=myHuffTree->Root();

                tempCh=tempCh<<1;
                continue;
            }
            if(((tempCh>>(leftNum-1))&0x01)==0x01){
                tempNode=((IntlNode<char>*)tempNode)->Right();
                tempCh=tempCh<<1;
            }
            else{
                tempNode=((IntlNode<char>*)tempNode)->Left();
                tempCh=tempCh<<1;
            }
        }
    }
    ifs.close();
    ofs.close();
}