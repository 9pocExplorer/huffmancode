//
// Created by Turing on 2022/11/12.
//
#include"compress.h"
#include<iostream>
#include"decompress.h"
using namespace std;
int main(){
    while(1){
        cout<<"1.Huffman compress."<<endl;
        cout<<"2.Huffman decompress."<<endl;
        cout<<"3.Exit."<<endl;
        cout<<"Please select:";
        char choice;
        cin>>choice;
        if(choice=='3'){
            break;
        }
        else if(choice=='1'){
            cout<<"Please input code file name:";
            char tempStr[20]={0};
            cin>>tempStr;
            Compress(tempStr);
            cout<<endl;
            cout<<"Pocessing..."<<endl;
            cout<<"Process end."<<endl;
            cout<<endl<<endl<<endl;
        }
        else{
            cout<<"Please input source file name(size less than 4GB):";
            char tempStr[20]={0};
            cin>>tempStr;
            Decompress(tempStr);

            cout<<endl;
            cout<<"Pocessing..."<<endl;
            cout<<"Process end."<<endl;
        }


    }

    return 0;
}

