//
// Created by Turing on 2022/11/8.
//
#pragma once

#include<iostream>

using namespace std;


template<class E, class comp>class Heap {
private:
    E* heapArr;//指向存放数据的数组
    int maxsize;
    int n;//当前堆上数据的个数
    comp cmp;
    void SiftDown(int pos) {
        while (!IsLeaf(pos)) {
            int j = LeftChild(pos);
            int rc = RightChild(pos);

            if (rc < n && cmp.prior(heapArr[rc] ,heapArr[j])) {//prior表示，第一个参数的优先级是否比第二个高
                j = rc;
            } //优先级比较，选出优先级更高的下标作为j的值。
            if (cmp.prior(heapArr[pos],heapArr[j]))//如果当前节点比它的两个子节点的优先级都要高，则表示当前的节点的位置正确。
                return;
            Swap(heapArr, j, pos);//如果当前节点没有它的子节点优先级高，则将优先级更高的子节点放上来。
            pos = j; //当交换完后，当前pos所指的为被交换的那个最高优先级的节点，因此，让pos指向我们需要siftdown的那个节点，
            // 继续与它的孩子比较
        }
    }//因此我的理解是，如果要将一个堆放到正确的位置，就必须从叶节点那一层的上一层开始，对每个父节点进行siftdown

public:
    Heap(E * h,int num,int max) {//传入指向该数组的首地址
        heapArr = h;
        n = num;
        maxsize = max;
        BuildHeap();
    }
    int Size() const {//在函数后面加上const禁止了该函数改变类的成员变量
        return n;
    }
    bool IsLeaf(int pos) const {
        return   (pos >= n / 2) && (pos < n); //不是很理解，但是盲猜，总节点数量的一半对于完全满二叉树来说，就是最底下一行的节点
    }
    int LeftChild(int pos)const {
        return 2 * pos + 1;
    }
    int RightChild(int pos) const {
        return 2 * pos + 2;
    }
    int Parent(int pos)const {
        return (pos - 1) / 2;
    }
    void BuildHeap() {//跟我想的一样，n/2-1代表的是最后一个非叶节点，即它在数组的下一个值就为该堆的叶节点。
        for(int i=n/2-1;i>=0;i--){
            SiftDown(i);
        }
    }

    void Swap(E* arr,int pos1,int pos2) {//交换堆中的两个节点的值
        E temp;
        temp =arr[pos1];
        arr[pos1]=arr[pos2];
        arr[pos2]=temp;
    }

    void Insert(const E& it)  {
        if (n >= maxsize) {//当当前数组的元素的个数大于容量的时候，插入失败
            cout<<"Heap is full"<<endl;
            return ;
        }
        int curr=n++;
        heapArr[curr] = it;
        //然后开始把插入的该元素放到他正确的位置,方法是与它的父节点进行比较，给兄弟节点没有关系
        while((curr!=0)&&cmp.prior(heapArr[curr],heapArr[Parent(curr)])) {
            Swap(heapArr,curr, Parent(curr));
            curr = Parent(curr);
        }
    }

    //将最高优先级的元素取出来
    E RemoveFirst(){
        if(n<=0){
            cout<<"Heap is empty"<<endl;
        }
        Swap(heapArr,0,--n);//将第一个元素与最后一个元素交换，并把该元素放到正确的位置，并且n--后相当于删除了
        //被放到最后一个位置的元素.
        if(n!=0){
            SiftDown(0);
        }
        return heapArr[n];
    }

    E Remove(int pos) {
        if (pos < 0 || pos >= n) {
            cout << "Bad position" << endl;
        }
        if(pos==n-1) {
            n--;
        }
        else {
            Swap(heapArr, pos, --n);
            while(pos!=0&&comp::prior(heapArr[pos],heapArr[Parent(pos)])){
                //这里不像取出优先级最高的操作那样，是因为，交换到该位置的元素的优先级不一定像第0个位置那样优先级一定是最高的，
                // 因为可能不在同一颗树上，最后一位元素的优先级可能比其他树中层次比他低的元素的优先级更高。因此交换后该元素的优先级可能
                // 比它的父元素高。
                Swap(heapArr,pos, Parent(pos));
                pos = Parent(pos);
            }

            if(n!=0){ //为什么将该元素与所有父元素比较后，仍然要进行siftdown，
                SiftDown(pos);
            }
        }
        return heapArr[n];
    }
};
