#pragma once
#include<iostream>
template<class E> class link {
public:
	E element;
	link* next;
	link() {
		next = NULL;
	}
	link(const E& it, link* nextval) {
		element = it;
		next = nextval;
	}
};


